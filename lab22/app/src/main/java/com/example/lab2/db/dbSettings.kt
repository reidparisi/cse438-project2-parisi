package com.example.lab2.db

import android.provider.BaseColumns

class dbSettings {
    companion object {
        const val DB_NAME = "songs.db"
        const val DB_VERSION = 1
    }
    //only the artist and song is stored in the db because only those need to be displayed in the
    //playlist, so we can minimize space by only storing those
    class DBSongEntry: BaseColumns {
        companion object {
            const val TABLE = "fav_songs"
            const val ID = BaseColumns._ID
            const val COL_ARTIST = "artist"
            const val COL_SONG = "song"
        }
    }
}