package com.example.lab2.viewModel

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.lab2.R
import com.example.lab2.songs.song
import kotlinx.android.synthetic.main.activity_secondary.*
//import java.util.Observer
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.widget.Button
import android.widget.Toast
import com.example.lab2.db.dbHelper
import com.example.lab2.db.dbSettings
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.fragment_playlist.view.*

class playlistFrag(context: Context) {
    var context=context

    @SuppressLint("ValidFragment")
    class playlists(context: Context) : Fragment() {
        var context2=context
        private var adapter = JokeAdapter()
        private lateinit var viewModel: playlistViewModel
        private var SongList: ArrayList<song> = ArrayList()

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.activity_secondary, container, false)
        }

        override fun onStart() {
            super.onStart()

            result_items_list.layoutManager = LinearLayoutManager(result_items_list.context)
            result_items_list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            viewModel = ViewModelProviders.of(this).get(playlistViewModel::class.java)
            result_items_list.adapter = adapter
            val observer = Observer<ArrayList<song>> {
                result_items_list.adapter = adapter
                //how to check if the dataset has changed
                val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                    override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                        if(p0 >= SongList.size || p1 >= SongList.size) {
                            return false;
                        }
                        return (SongList[p0].getArtist()== SongList[p1].getArtist())&& (SongList[p0].getName()==SongList[p1].getName())
                    }

                    override fun getOldListSize(): Int {
                        return SongList.size
                    }

                    override fun getNewListSize(): Int {
                        if (it == null) {
                            return 0
                        }
                        return it.size
                    }

                    override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                        return SongList[p0] == SongList[p1]
                    }
                })
                result.dispatchUpdatesTo(adapter)
                SongList = it ?: ArrayList()
            }

            //if this dataset changes then update the view
            viewModel.getSongsFromDatabase().observe(this, observer)
        }

        override fun setUserVisibleHint(isVisibleToUser: Boolean) {
            super.setUserVisibleHint(isVisibleToUser)
            if (isVisibleToUser) {
                fragmentManager!!.beginTransaction().detach(this).attach(this).commit()
            }
        }

        inner class JokeAdapter: RecyclerView.Adapter<JokeAdapter.JokeViewHolder>() {

            override fun onCreateViewHolder(p0: ViewGroup, p1: Int): JokeViewHolder {
                val itemView = LayoutInflater.from(p0.context).inflate(R.layout.fragment_playlist, p0, false)
                return JokeViewHolder(itemView)
            }
            //what to do with each element
            override fun onBindViewHolder(p0: JokeViewHolder, p1: Int) {
                //get the song from the songlist and populate the fields with that data
                val joke = SongList[p1]
                p0.songName.text = "song: "+joke.getName()
                p0.songArtist.text = "artist: " + joke.getArtist()
                //if button is clicked delete song from playlist and update view
                p0.button.setOnClickListener {
                        val db = dbHelper(context2)
                        db.deleteJoke(joke.getName(),joke.getArtist())
                    val text = "song deleted from playlist"
                    val duration = Toast.LENGTH_SHORT
                    val toast = Toast.makeText(context, text, duration)
                    toast.show()
                    viewModel.getSongsFromDatabase()
                    }
            }

            override fun getItemCount(): Int {
                return SongList.size
            }

            inner class JokeViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
                var songName: TextView = itemView.songName
                var songArtist: TextView = itemView.songArtist
                var button: Button = itemView.delete_playlist
            }
        }
    }





}