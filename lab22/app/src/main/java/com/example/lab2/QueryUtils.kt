package com.example.lab2


import android.text.TextUtils
import android.util.Log
import com.example.lab2.songs.song
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.nio.charset.Charset
import kotlin.collections.ArrayList

class QueryUtils {
    companion object {
        private val LogTag = this::class.java.simpleName
        private const val BaseURL = "http://ws.audioscrobbler.com/2.0/"
        //called by playlistViewModel. This will return the data from the api query and decide which
        //api call to make, and also makes the http request from the query string
        fun fetchProductData(jsonQueryString: String): ArrayList<song>? {
            val url: URL? = createUrl("${this.BaseURL}$jsonQueryString")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
            } catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }
            if(jsonQueryString.contains("artist")) {
                var songs = extractSongFromJson(jsonResponse)
                return songs
            }
            else {
                var songs= extractDataFromJson(jsonResponse)
                return songs
            }
        }
        //creates the url from the string url
        private fun createUrl(stringUrl: String): URL? {
            var url: URL? = null
            try {
                url = URL(stringUrl)
            } catch (e: MalformedURLException) {
                Log.e(this.LogTag, "Problem building the URL.", e)
            }

            return url
        }
        // makes https request from the url and returns the json response read from the api
        private fun makeHttpRequest(url: URL?): String {
            var jsonResponse = ""

            if (url == null) {
                return jsonResponse
            }

            var urlConnection: HttpURLConnection? = null
            var inputStream: InputStream? = null
            try {
                urlConnection = url.openConnection() as HttpURLConnection
                urlConnection.readTimeout = 10000 // 10 seconds
                urlConnection.connectTimeout = 15000 // 15 seconds
                urlConnection.requestMethod = "GET"
                urlConnection.connect()

                if (urlConnection.responseCode == 200) {
                    inputStream = urlConnection.inputStream
                    jsonResponse = readFromStream(inputStream)
                } else {
                    Log.e(this.LogTag, "Error response code: ${urlConnection.responseCode}")
                }
            } catch (e: IOException) {
                Log.e(this.LogTag, "Problem retrieving the product data results: $url", e)
            } finally {
                urlConnection?.disconnect()
                inputStream?.close()
            }

            return jsonResponse
        }
        //reads the data that is passed from the api and puts it in a string
        private fun readFromStream(inputStream: InputStream?): String {
            val output = StringBuilder()
            if (inputStream != null) {
                val inputStreamReader = InputStreamReader(inputStream, Charset.forName("UTF-8"))
                val reader = BufferedReader(inputStreamReader)
                var line = reader.readLine()
                while (line != null) {
                    output.append(line)
                    line = reader.readLine()
                }
            }

            return output.toString()
        }
        //extracts data from the json string data and formats it into an array of songs if not searching
        //by artist
        private fun extractDataFromJson(productJson: String?): ArrayList<song>? {
            if (TextUtils.isEmpty(productJson)) {
                return null
            }

            val popSongsArrayList = ArrayList<song>()
            try {

                var baseJsonResponse = JSONObject(productJson)
                if(baseJsonResponse.has("error")){
                    return popSongsArrayList
                }
                else {
                    var tracks = baseJsonResponse.getJSONObject("tracks")
                    var trac = returnValueOrDefault<JSONArray>(tracks, "track") as JSONArray
                    if (trac != null) {
                        var num = 20
                        //this will take the first 20 or all of them if less
                        if (trac.length() < num) {
                            num = trac.length()
                        }
                        for (k in 0 until num) {
                            var trac2 = trac.getJSONObject(k)
                            val title = returnValueOrDefault<String>(trac2, "name") as String
                            val imgJSON = returnValueOrDefault<JSONArray>(trac2, "image") as JSONArray
                            val playcount = returnValueOrDefault<String>(trac2, "playcount") as String
                            val artistJson = returnValueOrDefault<JSONObject>(trac2, "artist") as JSONObject
                            val artist = artistJson.getString("name")
                            val listeners = returnValueOrDefault<String>(trac2, "listeners") as String
                            val img = imgJSON?.getJSONObject(imgJSON.length() - 1)
                            val imgPath = returnValueOrDefault<String>(img, "#text") as String
                            popSongsArrayList.add(
                                song(title, artist, imgPath, playcount, listeners)
                            )
                        }
                    }
                }

            } catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }
            return popSongsArrayList
        }
        //extracts data from the json string data and formats it into an array of songs if searching
        //by artist
        private fun extractSongFromJson(productJson: String?):  ArrayList<song>?{
            if (TextUtils.isEmpty(productJson)) {
                return null
            }
            val popSongsArrayList = ArrayList<song>()

            try {

                var baseJsonResponse = JSONObject(productJson)

                if(baseJsonResponse.has("error")){
                    return popSongsArrayList
                }
                else {

                    var tracks = baseJsonResponse.getJSONObject("toptracks")
                    var trac = returnValueOrDefault<JSONArray>(tracks, "track") as JSONArray


                   //will take top 20 songs or all of them if less than 20
                    if (trac != null) {
                        for (k in 0 until 19) {
                            var trac2 = trac.getJSONObject(k)

                            val title = returnValueOrDefault<String>(trac2, "name") as String
                            val imgJSON = returnValueOrDefault<JSONArray>(trac2, "image") as JSONArray
                            val playcount = returnValueOrDefault<String>(trac2, "playcount") as String
                            val artistJson = returnValueOrDefault<JSONObject>(trac2, "artist") as JSONObject
                            val artist = artistJson.getString("name")
                            val listeners = returnValueOrDefault<String>(trac2, "listeners") as String
                            val img = imgJSON?.getJSONObject(imgJSON.length() - 1)
                            val imgPath = returnValueOrDefault<String>(img, "#text") as String
                            popSongsArrayList.add(
                                song(title, artist, imgPath, playcount, listeners)
                            )
                        }

                    }
                }
            } catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }
            return popSongsArrayList
        }

        //will return empty value or will return the value if it can from the json object with key given
        private inline fun <reified T> returnValueOrDefault(json: JSONObject, key: String): Any? {
            when (T::class) {
                String::class -> {
                    return if (json.has(key)) {
                        json.getString(key)
                    } else {
                        ""
                    }
                }
                Int::class -> {
                    return if (json.has(key)) {
                        json.getInt(key)
                    } else {
                        return -1
                    }
                }
                Double::class -> {
                    return if (json.has(key)) {
                        json.getDouble(key)
                    } else {
                        return -1.0
                    }
                }
                Long::class -> {
                    return if (json.has(key)) {
                        json.getLong(key)
                    } else {
                        return (-1).toLong()
                    }
                }
                JSONObject::class -> {
                    return if (json.has(key)) {
                        json.getJSONObject(key)
                    } else {
                        return null
                    }
                }
                JSONArray::class -> {
                    return if (json.has(key)) {
                        json.getJSONArray(key)
                    } else {
                        return null
                    }
                }
                else -> {
                    return null
                }
            }
        }
    }
}
