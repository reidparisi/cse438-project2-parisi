package com.example.lab2.viewModel

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.os.AsyncTask
import android.provider.BaseColumns
import android.util.Log
import com.example.lab2.QueryUtils
import com.example.lab2.db.*
import com.example.lab2.songs.song

class playlistViewModel (application: Application): AndroidViewModel(application) {
    public var _songList: MutableLiveData<ArrayList<song>> = MutableLiveData()
    private var _songsDBHelper: dbHelper = dbHelper(application)
    public var recall_data:Boolean=false

    //query top songs from the api
    fun getTopSongs(query: String): MutableLiveData<ArrayList<song>> {
        ProductAsyncTask().execute(query)
        return this._songList
    }
    //query the top songs by a given artist from the api or if the artist is not the input text
    //then try to draw the top songs from the country that was input
    fun getArtistSongs(query: String,text:String): MutableLiveData<ArrayList<song>> {
        if(!query.contains('*')) {
            ProductAsyncTask().execute(query)
        }
        else{

       // if(this.recall_data) {
            val country=text.drop(1)
            val query2: String =
                "?method=geo.gettoptracks&country=" + country + "&api_key=20130afe06e82babb3e4a5a5d4979f56&format=json"
            ProductAsyncTask().execute(query2)
            this.recall_data = false
        }
        return this._songList
    }

    //asynchronously query the api and when the result is found update the songlist to be displayed
    @SuppressLint("StaticFieldLeak")
    inner class ProductAsyncTask : AsyncTask<String, Unit, ArrayList<song>>() {
        override fun doInBackground(vararg params: String?): ArrayList<song>? {
            return QueryUtils.fetchProductData(params[0]!!)

        }

        override fun onPostExecute(result: ArrayList<song>?) {
            if (result?.size == 0) {
                Log.e("RESULTS", "No Results Found")
            } else {
                this@playlistViewModel._songList.value = result

            }
        }
    }
        //get the songs stored in the playlist from the db
        fun getSongsFromDatabase(): MutableLiveData<ArrayList<song>> {
            loadSongsForDB()
            return _songList
        }

        //load in the songs from the db
        private fun loadSongsForDB() {
            val db = _songsDBHelper.readableDatabase

            // Define a projection that specifies which columns from the database
            // you will actually use after this query.
            val projection =
                arrayOf(BaseColumns._ID, dbSettings.DBSongEntry.COL_ARTIST, dbSettings.DBSongEntry.COL_SONG)

            val cursor = db.query(
                dbSettings.DBSongEntry.TABLE,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                null,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                null               // The sort order
            )

            var songs = ArrayList<song>()
            with(cursor) {
                while (moveToNext()) {
                    val artist = getString(getColumnIndexOrThrow(dbSettings.DBSongEntry.COL_ARTIST))
                    val title = getString(getColumnIndexOrThrow(dbSettings.DBSongEntry.COL_SONG))
                    val j = song(title, artist, "", "", "")
                    songs.add(j)
                }
            }
            _songList.value = songs
        }

        private fun getFavoriteBits() {
            val favourites: ArrayList<song> = ArrayList()

        }

    }
