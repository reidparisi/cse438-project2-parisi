package com.example.lab2

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.lab2.db.dbHelper
import com.example.lab2.songs.song
import com.squareup.picasso.Picasso

import kotlinx.android.synthetic.main.song_view.*


class displaySong : AppCompatActivity() {
    private lateinit var song1: song
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.song_view)

        //finds all the views to use later
        val button = findViewById<Button>(R.id.playlist_add)
        val name: TextView = findViewById<TextView>(R.id.songName)
        val artist: TextView = findViewById<TextView>(R.id.artist)
        val listeners: TextView = findViewById<TextView>(R.id.songListeners)
        val playcount: TextView = findViewById<TextView>(R.id.songPlaycount)
        val img: ImageView = findViewById<ImageView>(R.id.songImage)
        var song1 = intent.extras!!.getSerializable("song") as song

        //setting all the text fields from the song data passed from the intent
        name.text = "song: "+song1.getName()
        artist.text="artist: "+song1.getArtist()
        if(song1.getPlayCount().isEmpty()){
            playcount.text="info not available in country search"
        }
        else {
            playcount.text = "play count: " + song1.getPlayCount()
        }
        listeners.text = "listeners: "+song1.getListeners()
        if(song1.getImg_url().endsWith(".png")) {
            Picasso.with(this).load(song1.getImg_url()).into(img)
        }
        val db = dbHelper(this)
        //see if the song is in the database. If it is we hide the button so they can't add it again
        val nameOrNull = db.getSongOrNull(song1.getName(),song1.getArtist())
        if(nameOrNull!=null){
            button.visibility= View.GONE
        }
        else{
            button.visibility=View.VISIBLE
        }
        //if the button is clicked add the song to the playlist and display toast
        button.setOnClickListener {
            db.addJoke(song1.getName(),song1.getArtist())
            val toast = Toast.makeText(this, "song added", Toast.LENGTH_SHORT)
            toast.show()
            setResult(RESULT_OK, intent);
            this.finish()
         }
    }
    override fun onBackPressed() {
        this.finish()
    }
}