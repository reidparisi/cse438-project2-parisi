package com.example.lab2.viewModel

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.lab2.QueryUtils.Companion.fetchProductData
import com.example.lab2.R
import com.example.lab2.songs.song
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_secondary.*
import kotlinx.android.synthetic.main.fragment_griditem.view.*
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.song_view.*
import android.widget.AdapterView.OnItemClickListener
import android.widget.Button
import com.example.lab2.displaySong
import android.view.View.OnFocusChangeListener
import android.media.MediaPlayer
import android.util.Log

import android.widget.EditText
import java.lang.Package.getPackage
import java.lang.Package.getPackages


 class homeFrag() {


    @SuppressLint("ValidFragment")
     class home(contexter: Context) : Fragment() {
        private lateinit var mp: MediaPlayer
        private var parentContext: Context = contexter
        private var adapter = HomeAdapter()
        private lateinit var viewModel: playlistViewModel
        private var once =false
        private var SongList: ArrayList<song> = ArrayList()

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            //start to play thank you next


            return inflater.inflate(com.example.lab2.R.layout.fragment_main, container, false)

        }




        override fun onStart() {
            super.onStart()


            //set columns to two in the recycler view
            val gridLayoutManager = GridLayoutManager(parentContext, 2)
            recyclerMain.setLayoutManager(gridLayoutManager)
            //recyclerMain.layoutManager = LinearLayoutManager(this.context)
            recyclerMain.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            viewModel = ViewModelProviders.of(this).get(playlistViewModel::class.java)
            recyclerMain.adapter=adapter
            val observer = Observer<ArrayList<song>> {
                recyclerMain.adapter=adapter
                //how to see if items are different
                val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                    override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                        if (p0 >= SongList.size || p1 >= SongList.size) {
                            return false;
                        }
                        return (SongList[p0].getArtist() == SongList[p1].getArtist()) && (SongList[p0].getName() == SongList[p1].getName())
                    }

                    override fun getOldListSize(): Int {
                        return SongList.size
                    }

                    override fun getNewListSize(): Int {
                        if (it == null) {
                            return 0
                        }
                        return it.size
                    }

                    override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                        return SongList[p0] == SongList[p1]
                    }
                })
                result.dispatchUpdatesTo(adapter)
                SongList = it ?: ArrayList()
            }
            //top tracks query string
            var query: String =
                "?method=chart.gettoptracks&api_key=20130afe06e82babb3e4a5a5d4979f56&format=json"

            viewModel.getTopSongs(query).observe(this, observer) //is this the right owner

            //found on stack overflow, but altered. This will do some action if the textbow
            //changes its focus from being clicked so someone is typing to someone clicking
            //off of the textbox
            var ai = artist_input
               ai.onFocusChangeListener =
                    OnFocusChangeListener { v, hasFocus ->
                        /* When focus is lost check that the text field
                            * has valid values and then query depending on if text was input
                            */
                        if (!hasFocus) {
                            if(ai.text==null){
                                viewModel.getTopSongs(query).observe(this, observer)
                            }
                            else{
                                query="?method=artist.gettoptracks&artist=" +
                                        ai.text +
                                        "&api_key=20130afe06e82babb3e4a5a5d4979f56&format=json"
                                viewModel.getArtistSongs(query,ai.text.toString()).observe(this, observer)
                            }

                        }
                        mp = MediaPlayer()
                        var resId=getResources().getIdentifier("thank_u_next","raw", this@home.context?.packageName)
                        mp=MediaPlayer.create(this@home.context,resId)
                        if(!once) {
                            once = true
                            mp.start()
                        }
                    }

        }
            override fun setUserVisibleHint(isVisibleToUser: Boolean) {
                super.setUserVisibleHint(isVisibleToUser)
                if (isVisibleToUser) {
                    fragmentManager!!.beginTransaction().detach(this).attach(this).commit()
                }
            }
            //recycler view adapter
            inner class HomeAdapter : RecyclerView.Adapter<HomeAdapter.JokeViewHolder>() {

                override fun onCreateViewHolder(p0: ViewGroup, p1: Int): JokeViewHolder {
                    val itemView = LayoutInflater.from(p0.context).inflate(R.layout.fragment_griditem, p0, false)
                    return JokeViewHolder(itemView)
                }
                //what to do with each element
                override fun onBindViewHolder(p0: JokeViewHolder, p1: Int) {
                    //gets joke aka song from the songlist and fills the fields of the itemView
                    //with the song data from the array
                    val joke = SongList[p1]
                    p0.songName.text = joke.getName()
                    if(joke.getImg_url().endsWith(".png")) {
                        Picasso.with(p0.itemView.context).load(joke.getImg_url()).into(p0.albumPic)
                    }
                    p0.row.setOnClickListener {
                        val intent = Intent(p0.itemView.context, com.example.lab2.displaySong::class.java)
                        intent.putExtra("song", joke)
                        startActivity(intent)
                    }
                }



                override fun getItemCount(): Int {
                    return SongList.size
                }

                inner class JokeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
                    var albumPic: ImageView = itemView.album_pic_frag
                    var songName: TextView = itemView.song_name_frag
                    var row = itemView

                }
            }
        }


    }
