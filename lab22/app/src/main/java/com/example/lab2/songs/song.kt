package com.example.lab2.songs
import java.io.Serializable
import kotlin.collections.ArrayList
class song(): Serializable {
    private var name: String = ""       //name of song
    private var artist: String = ""     //song artist
    private var img_url: String = ""    //url to album image
    private var playcount: String = ""  //how many times its been played
    private var listeners: String = ""  //how many listeners the song has

    constructor(
        name: String,
        artist: String,
        img_url: String,
        playcount: String,
        listeners: String
    ) : this() {
        this.name = name
        this.artist = artist
        this.img_url = img_url
        this.playcount = playcount
        this.listeners = listeners
    }

    fun getName(): String {
        return this.name
    }
    fun getArtist(): String {
        return this.artist
    }
    fun getImg_url(): String {
        return this.img_url
    }
    fun getPlayCount(): String {
        return this.playcount
    }
    fun getListeners(): String {
        return this.listeners
    }
}