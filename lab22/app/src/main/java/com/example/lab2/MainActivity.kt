package com.example.lab2

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import android.content.Context
import android.net.ConnectivityManager
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.widget.GridLayoutManager
import com.example.lab2.viewModel.homeFrag
import com.example.lab2.viewModel.playlistFrag
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fragmentAdapter = MyPagerAdapter(supportFragmentManager, this)
        viewpager_main.adapter = fragmentAdapter

        tabs_main.setupWithViewPager(viewpager_main)
        //added this \>


    }

    class MyPagerAdapter(fm: FragmentManager, context: Context) : FragmentPagerAdapter(fm) {
        var context=context
        //gets which tab you are on and calls that method to inflate the fragment
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    homeFrag.home(context)
                }
                else -> playlistFrag.playlists(context)
            }
        }

        override fun getCount(): Int {
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> "HOME"
                else -> {
                    return "PLAYLISTS"
                }
            }
        }
    }
}
