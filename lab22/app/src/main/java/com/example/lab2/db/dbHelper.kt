package com.example.lab2.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class dbHelper (context: Context): SQLiteOpenHelper(context, dbSettings.DB_NAME, null, dbSettings.DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        val createFavoritesTableQuery = "CREATE TABLE " + dbSettings.DBSongEntry.TABLE + " ( " +
                dbSettings.DBSongEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                dbSettings.DBSongEntry.COL_ARTIST + " TEXT NULL, " +
                dbSettings.DBSongEntry.COL_SONG + " TEXT NULL)"

        db?.execSQL(createFavoritesTableQuery)


    }
    //upgrades are not implemented so deleting data should be fine
    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS " + dbSettings.DBSongEntry.TABLE)
        onCreate(db)
    }
    //by joke I mean song, and adds the song to the database
    fun addJoke(song: String, artist: String) {
        // Gets the data repository in write mode
        val db = this.writableDatabase

        // Create a new map of values, where column names are the keys
        val values = ContentValues().apply {
            put(dbSettings.DBSongEntry.COL_SONG, song)
            put(dbSettings.DBSongEntry.COL_ARTIST, artist)
        }

// Insert the new row, returning the primary key value of the new row
        val newRowId = db?.insert(dbSettings.DBSongEntry.TABLE, null, values)
    }
    //delete the song from the database if it exists
    fun deleteJoke(song: String, artist: String){
        var os = song
        var ns = ""
        var oa = artist
        var na = ""
        val db = this.writableDatabase
        //if the artist or song has ' in its name it will throw an error in the sql query so
        //here I add a second ' to make the query text correct
        if(artist.contains("\'")) {
            for(c in artist){
                if(c!='\''){
                    na+=c
                }
                else{
                    na+=c
                    na+="\'"
                }
            }
            oa=na
        }
        if(song.contains("\'")){
            for(c in song){
                if(c!='\''){
                    ns+=c
                }
                else{
                    ns+=c
                    ns+="\'"
                }
            }
            os=ns
        }
                var sqlString: String = "DELETE FROM fav_songs WHERE artist='"+oa+
                        "' and song='"+os+"';"
                db.execSQL(sqlString)

    }
    fun getSongOrNull(song: String, artist: String):String?{
        var ns:String = ""
        var na:String=""
        var os = song
        var oa: String = artist
        val db = this.readableDatabase
        var bindArgs:String
        //if the artist or song has ' in its name it will throw an error in the sql query so
        //here I add a second ' to make the query text correct
        if(artist.contains("\'")) {
            for(c in artist){
                if(c!='\''){
                    na+=c
                }
                else{
                    na+=c
                    na+="\'"
                }
            }
            oa=na
        }
        if(song.contains("\'")){

            for(c in song){
                if(c!='\''){
                    ns+=c
                }
                else{
                    ns+=c
                    ns+="\'"
                }
            }
            os=ns
        }


        var sqlString: String? = "SELECT * FROM "+dbSettings.DBSongEntry.TABLE+" WHERE artist='"+oa+
                "' and song='"+os+"';"
        var c:Cursor = db.rawQuery(sqlString, null)
        c.moveToFirst()

        if(c.count==0||c.isNull(c.getColumnIndex("song"))){
            return null
        }
        else {
            return c.getString(c.getColumnIndex("song"))
        }
    }
}