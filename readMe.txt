I attribute Last.fm API as source of data
I give credit to Ariana Grande for her creation of "Thank You Next" which plays in background upon search

I used Genymotion Google Pixel 3, and it was all functioning when I last used it. There were occasionally issues on start up that were solved
with cleaning the project then rebuilding and starting again, but I was told this shouldn't be an issue. 


Creative: For my creative portion I enabled the ability to search countries by sond. Initially I tried implementing it where they would input
say "honduras" and it would decide if it was a song or a country, but it turns out that every country I tested also was an artist name,
even Azjerbijan. Thus I implemented it so that if the user types a "*" before their search it will display the top songs by country. I
implemented this function because it gives another aspect of searching for songs. Users might want to see the trending songs in their home
nation. I implemented this by making a different call to the api to get top songs by country and filling the song array in this way. This query
does not give information on playcount so that is left empty, but all other information is displayed.

I also implemented a song by Ariana Grande, "Thank You Next" to play when the user clicks on the search query text. I implemented this
because I felt that for a music app to not be able to play music was not optimal so I played a song. I felt to play each song would cause too 
much local storage on the phone, and that would incur copyright issues. Thus I played one song "Thank You Next" so that it plays in the background
while the user is browsing. I implement this by making a MediaPlayer and accessing a downloaded .jpg in the raw folder of resources to play upon
changing focus of the search bar. 

FROM INSTRUCTOR:
98/100

0/1 No Attribution
3/4 When keybaord opens, grdview covers search field